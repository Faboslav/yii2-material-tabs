<?php

namespace faboslav\materialtabs;

use yii\base\InvalidConfigException;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class MaterialTabs extends Widget
{
    public static $autoIdPrefix = 'material-tabs-';

    public $items = [];

    /**
     * @var boolean whether to enable ripple effect on the tabs header items.
     */
    public $ripple = true;

    /**
     * @var boolean whether to enable arrows for scrolling between header items.
     */
    public $arrows = true;

    /**
     * @inheritdoc
     */
    public function run() {
        $this->initWidget();
        echo $this->renderTabs();
    }

    /**
     * Initializes the widget settings.
     */
    public function initWidget()
    {
        Html::addCssClass($this->options, ['widget' => 'material-tabs']);

        $this->registerAssets();
    }

    /**
     * Registers the assets.
     */
    public function registerAssets()
    {
        $view = $this->getView();

        MaterialTabsAsset::register($view);

        $id = '#' . $this->options['id'];

        $view->registerJs(<<<JS
            jQuery('{$id}').materialTabs().init();
JS
        );

        if($this->arrows) {
            $view->registerJs(<<<JS
                jQuery('{$id}').materialTabs().checkViewport();

                $(".viewport").scroll(function () {
                    jQuery('{$id}').materialTabs().checkViewport();
                });
        
                $(window).resize(function () {
                    jQuery('{$id}').materialTabs().checkViewport();
                });
                
                $(document).on('click', '{$id} .left-arrow', function (e) {
                    $('{$id} nav .viewport').animate({scrollLeft: $("{$id} nav .viewport").scrollLeft() - 150}, 150, 'swing');
                });
        
                $(document).on('click', '{$id} .right-arrow', function (e) {
                    $('{$id} nav .viewport').animate({scrollLeft: $("{$id} nav .viewport").scrollLeft() + 150}, 150, 'swing');
                });
JS
            );
        }

        $view->registerJs(<<<JS
            $(document).on('click', '{$id} nav li', function () {
                if (!jQuery('{$id}').materialTabs().animating) {
                    jQuery('{$id}').materialTabs().animating = true;
                    jQuery('{$id}').materialTabs().setActiveItem($(this));
                    
                    setTimeout(function () {
                        jQuery('{$id}').materialTabs().animating = false;
                        $('{$id} .tabs .tab-content').removeAttr("style");
                    }, 500);
                }
            });

            $(window).resize(function () {
                activeItem = $('{$id} nav li.active');
    
                if (activeItem.length) {
                    jQuery('{$id}').materialTabs().setBorder(activeItem);
                }
            });
JS
        );

        if($this->ripple) {
            $selector = "jQuery('{$id} nav .viewport ul li')";
            $view->registerJs("{$selector}.materialRipple();");
        }
    }

    public function renderTabs() {
        return Html::tag('div', $this->renderHeaderItems().$this->renderContentItems(), $this->options);
    }

    public function renderHeaderItems() {
        $items = [];

        foreach ($this->items as $i => $item) {
            $items[] = $this->renderHeaderItem($i, $item);
        }

        $list = Html::tag('ul', implode("\n", $items));

        $leftFade = Html::tag('div', Html::tag('div', '', ['class' => 'left-arrow']), ['class' => 'left-fade']);
        $viewport = Html::tag('div', $list, ['class' => 'viewport']);
        $rightFade = Html::tag('div', Html::tag('div', '', ['class' => 'right-arrow']), ['class' => 'right-fade']);

        return Html::tag('nav', $leftFade.$viewport.$rightFade);
    }

    public function renderHeaderItem($i, $item) {
        if (is_string($item)) {
            return $item;
        }

        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }

        $options = ArrayHelper::getValue($item, 'options', []);
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        $url = ArrayHelper::getValue($item, 'url', '#');

        if (isset($item['active']) && $item['active']) {
            Html::addCssClass($options, 'active');
        }

        return Html::tag('li', $item['label'], merge($options, [
            'data' => [
                'tab' => ($i+1)
            ]
        ]));
    }

    /**
     * Renders tabs items
     *
     * @return string the rendering result.
     */
    public function renderContentItems()
    {
        $items = [];

        foreach ($this->items as $i => $item) {
            $items[] = $this->renderContentItem($i, $item);
        }
        return Html::tag('div', implode("\n", $items), [
            'class' => 'tabs'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function renderContentItem($i, $item)
    {
        if (!isset($item['content'])) {
            throw new InvalidConfigException("The 'content' option is required.");
        }

        $options = ArrayHelper::getValue($item, 'options', []);
        $contentItemOptions = ArrayHelper::getValue($item, 'contentItemOptions', []);

        Html::addCssClass($contentItemOptions, 'tab-content');

        if (isset($item['active']) && $item['active']) {
            Html::addCssClass($contentItemOptions, 'active');
        }

        return Html::tag('div', $item['content'], merge($contentItemOptions, [
            'data' => [
                'tab' => ($i+1)
            ]
        ]));
    }
}